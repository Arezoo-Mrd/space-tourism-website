/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {

    fontSize: {
      xs: '14px',
      sm: '16px',
      md: '18px',
      lg: '28px',
      xl: '32px',
      '2xl': '56px',
      '3xl': '100px',
      '4xl': '150px',
    },
    fontFamily: {
      BarlowCondensed: ['BarlowCondensed'],
      BelleFair: ['BelleFair']
    },
    extend: {
      colors: {
        black: '#0B0D17',
        blackLight: '#24262f',
        gray: '#D0D6F9',
        white: '#fff'
      },
    },
  },
  plugins: [],
}
