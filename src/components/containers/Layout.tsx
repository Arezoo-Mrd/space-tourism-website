import BgHomeDesktop from './../../assets/home/background-home-desktop.jpg';
import BgHomeTablet from './../../assets/home/background-home-tablet.jpg';
import BgHomeMobile from './../../assets/home/background-home-mobile.jpg';

import BgDescriptionDesktop from './../../assets/destination/background-destination-desktop.jpg';
import BgDescriptionTablet from './../../assets/destination/background-destination-tablet.jpg';
import BgDescriptionMobile from './../../assets/destination/background-destination-mobile.jpg';

import BgCrewDesktop from './../../assets/crew/background-crew-desktop.jpg';
import BgCrewTablet from './../../assets/crew/background-crew-tablet.jpg';
import BgCrewMobile from './../../assets/crew/background-crew-mobile.jpg';

import BgTechnologyDesktop from './../../assets/technology/background-technology-desktop.jpg';
import BgTechnologyTablet from './../../assets/technology/background-technology-tablet.jpg';
import BgTechnologyMobile from './../../assets/technology/background-technology-mobile.jpg';


import Header from '../common/Header'
import { useLocation } from 'react-router-dom'
import { Fragment } from 'react';

interface ILayoutProps {
    children: React.ReactNode
}
interface IBackgroundImage {
    href: string;
    screenSize: string;
    image: string;
}
const bgImage: IBackgroundImage[] = [
    { href: '/', screenSize: '1024', image: BgHomeDesktop },
    { href: '/', screenSize: '768', image: BgHomeTablet },
    { href: '/', screenSize: '300', image: BgHomeMobile },

    { href: '/destination', screenSize: '1024', image: BgDescriptionDesktop },
    { href: '/destination', screenSize: '768', image: BgDescriptionTablet },
    { href: '/destination', screenSize: '300', image: BgDescriptionMobile },

    { href: '/crew', screenSize: '1024', image: BgCrewDesktop },
    { href: '/crew', screenSize: '768', image: BgCrewTablet },
    { href: '/crew', screenSize: '300', image: BgCrewMobile },

    { href: '/technology', screenSize: '1024', image: BgTechnologyDesktop },
    { href: '/technology', screenSize: '768', image: BgTechnologyTablet },
    { href: '/technology', screenSize: '300', image: BgTechnologyMobile },
]

const Layout = ({ children }: ILayoutProps): JSX.Element => {

    const { pathname } = useLocation()


    const getImageSrc = (): IBackgroundImage[] => {
        const bgImagesFiltered = bgImage.filter(item => item.href === pathname)
        return bgImagesFiltered
    }


    return (
        <Fragment>
            <div className='hidden w-full h-screen overflow-hidden text-white bg-black bg-no-repeat bg-cover lg:block md:hidden' style={{ backgroundImage: `url(${getImageSrc()[0].image})` }}>
                <Header />
                {children}
            </div>
            <div className='hidden w-full h-screen overflow-hidden text-white bg-black bg-no-repeat bg-cover md:block lg:hidden' style={{ backgroundImage: `url(${getImageSrc()[1].image})` }}>
                <Header />
                {children}
            </div>
            <div className='w-full h-screen overflow-hidden text-white bg-black bg-no-repeat bg-cover md:hidden' style={{ backgroundImage: `url(${getImageSrc()[2].image})` }}>
                <Header />
                {children}
            </div>
        </Fragment>
    )
}

export default Layout