import { Fragment, useId } from 'react'
import data from './../../data/data.json'

const Destination = () => {
    const { destinations } = data
    const tabId = useId()
    return (
        <div className='flex items-center justify-center w-full h-full p-[165px] '>
            <div className='flex flex-col justify-center w-1/2 h-full '>
                <h5 className='text-lg tracking-[4.72px]'><span className="mr-2 text-gray ">01</span>Pick your destination</h5>
                <img src={(destinations[0].images.webp)} />
            </div>
            <div className='w-1/2 '>
                {destinations.map(destination => {
                    return (
                        <Fragment key={destination.name}>
                            <label className='text-sm tracking-[2.7px]' htmlFor={tabId}>{destination.name}</label>
                            <input type={'radio'} name='tabs' id={tabId} className='peer' />
                        </Fragment>
                    )
                })}

                {destinations.map(destination => {
                    return (
                        <div key={destination.description} className='hidden peer-checked:block'>{destination.description}</div>
                    )
                })}


            </div>
        </div >
    )
}

export default Destination