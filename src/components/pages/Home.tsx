import { useNavigate } from "react-router-dom";
import Button from '../common/Button'



const Home = (): JSX.Element => {
    const navigate = useNavigate()
    return (
        <div className='flex items-center justify-center w-full h-full p-[165px] '>
            <div className='flex flex-col justify-center h-full '>
                <h5 className='text-lg tracking-[4.72px]'>SO, YOU WANT TO TRAVEL TO</h5>
                <h1 className='text-4xl font-BelleFair'>SPACE</h1>
                <p className='w-2/3 text-md'>Let’s face it; if you want to go to space, you might as well genuinely go to outer space and not hover kind of on the edge of it. Well sit back, and relax because we’ll give you a truly out of this world experience!</p>
            </div>
            <div className='flex justify-end w-full'> <Button value='Explore' type='button' onClick={() => navigate('/destination')} /></div>
        </div>

    )
}

export default Home