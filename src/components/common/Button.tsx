type buttonType = 'button' | 'submit' | 'reset'
interface IButtonProps {
    value: string;
    type: buttonType;
    onClick?: () => void
}

const Button = ({ value, type, onClick }: IButtonProps): JSX.Element => {
    return (
        <div className='bg-transparent rounded-full w-[450px] h-[450px]  font-BelleFair flex justify-center items-center text-xl text-black   text-center   hover:bg-blackLight transition-[background] duration-500 hover:animate-pulse'>
            <button type={type} className='w-[275px] h-[275px] bg-white rounded-full outline-none' onClick={onClick}>{value.toUpperCase()}</button>
        </div>
    )
}

export default Button