import { IMenuItem } from "../models/menuItem"
import { Link, useLocation } from 'react-router-dom'
import { ReactComponent as Logo } from "./../../assets/shared/logo.svg"

const menuItems: IMenuItem[] = [
    { id: 0, title: 'Home', href: '/' },
    { id: 1, title: 'Destination', href: '/destination' },
    { id: 2, title: 'Crew', href: '/crew' },
    { id: 3, title: 'Technology', href: '/technology' },
]
const Header = () => {
    const { pathname } = useLocation()

    return (
        <div className='w-full lg:pt-10 '>
            <div className="flex items-center ">
                <span className="px-14">
                    <Link to='/'><Logo /></Link>
                </span>
                <div className="relative flex items-center justify-end w-full">
                    <div className="hidden lg:flex h-[1px] bg-gray w-1/2 relative left-10 z-10 opacity-50"></div>
                    <ul className="flex items-center w-2/3 h-full  tracking-[2.7px] justify-evenly  glass font-BarlowCondensed text-sm" >
                        {menuItems.map(item => <li className={`py-8 border-0 transition-all box-content hover:border-b-2 hover:border-gray ${item.href === pathname && 'border-b-2 border-white'}`} key={`Header__${item.id}`}><Link to={item.href}><span className="mr-2">0{item.id}</span>{item.title.toUpperCase()}</Link></li>)}
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default Header