export interface IMenuItem {
    id: number,
    title: string,
    href: string
}