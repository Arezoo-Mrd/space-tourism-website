import { Routes, Route } from 'react-router-dom';
import Layout from './components/containers/Layout';
import Crew from './components/pages/Crew';
import Destination from './components/pages/Destination';
import Home from './components/pages/Home';
import Technology from './components/pages/Technology';


function App() {
  return (
    <Layout >
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/destination' element={<Destination />} />
        <Route path='/crew' element={<Crew />} />
        <Route path='/technology' element={<Technology />} />
      </Routes>
    </Layout>
  );
}

export default App;
